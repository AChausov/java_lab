package com.javaLab.FirstLab;

import java.io.IOException;

public abstract class FileFormatReader {
    public FileFormatReader() {
    }

    public abstract float calculatedValueFromFiles() throws IOException;

    protected static float sumOfNumbers(float[] numbers) {
        float result = 0.0F;
        float[] var2 = numbers;
        int var3 = numbers.length;

        for(int var4 = 0; var4 < var3; ++var4) {
            float number = var2[var4];
            result += number;
        }

        return result;
    }

    protected static float multipleOfNumbers(float[] numbers) {
        float result = 1.0F;
        float[] var2 = numbers;
        int var3 = numbers.length;

        for(int var4 = 0; var4 < var3; ++var4) {
            float number = var2[var4];
            result *= number;
        }

        return result;
    }

    protected static float sumOfSquares(float[] numbers) {
        float result = 0.0F;
        float[] var2 = numbers;
        int var3 = numbers.length;

        for(int var4 = 0; var4 < var3; ++var4) {
            float number = var2[var4];
            result += number * number;
        }

        return result;
    }
}
