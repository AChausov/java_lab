package com.javaLab.FirstLab;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class FileFormatReaderSingleThread extends FileFormatReader {
    private float resultValue;
    private int filesCount;

    FileFormatReaderSingleThread(int filesCountValue) {
        this.filesCount = filesCountValue;
        this.resultValue = 0.0F;
    }

    public float calculatedValueFromFiles() throws IOException {
        for(int i = 0; i < this.filesCount; ++i) {
            BufferedReader input = new BufferedReader(new FileReader("in_" + i + ".dat"));
            int actionType = Integer.parseInt(input.readLine());
            String[] floatStrings = input.readLine().split(" ");
            float[] floats = new float[floatStrings.length];

            for(int j = 0; j < floats.length; ++j) {
                floats[j] = Float.parseFloat(floatStrings[j]);
            }

            input.close();
            switch(actionType) {
                case 1:
                    this.resultValue += sumOfNumbers(floats);
                    break;
                case 2:
                    this.resultValue += multipleOfNumbers(floats);
                    break;
                case 3:
                    this.resultValue += sumOfSquares(floats);
            }
        }

        return this.resultValue;
    }
}
