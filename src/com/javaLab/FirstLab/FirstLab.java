package com.javaLab.FirstLab;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class FirstLab {
    public FirstLab() {
    }

    public static void main(String[] args) throws IOException {
        int numberOfFiles = 0;

        try {
            numberOfFiles = getNumbersOfFiles();
            FileFormatWriter writer = new FileFormatWriter(numberOfFiles);
            writer.createFormatFiles();
        } catch (IOException var14) {
            System.err.println("IOException while generate files, " + var14.getLocalizedMessage());
        }

        try {
            long startTime = System.nanoTime();
            FileFormatReaderSingleThread singleThreadReader = new FileFormatReaderSingleThread(numberOfFiles);
            float singleThreadResult = singleThreadReader.calculatedValueFromFiles();
            long endTime = System.nanoTime();
            long duration = (endTime - startTime) / 1000000L;
            System.out.println("Single thread result - " + Float.toString(singleThreadResult));
            System.out.println("Single thread execution time - " + Long.toString(duration) + " ms");

            startTime = System.nanoTime();
            FileFormatReaderMultiThread multiThreadReader = new FileFormatReaderMultiThread(numberOfFiles);
            float multiThreadResult = multiThreadReader.calculatedValueFromFiles();
            endTime = System.nanoTime();
            duration = (endTime - startTime) / 1000000L;
            System.out.println("Multi thread result - " + Float.toString(multiThreadResult));
            System.out.println("Multi thread execution time - " + Long.toString(duration) + " ms");

            FileFormatWriter writer = new FileFormatWriter();
            writer.createResultFile(multiThreadResult, duration);
        } catch (IOException var13) {
            System.err.println("IOException while read files, " + var13.getLocalizedMessage());
        }

    }

    private static int getNumbersOfFiles() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter total numbers of files = ");
        int value = 0;

        try {
            value = Integer.parseInt(br.readLine());
        } catch (NumberFormatException var3) {
            System.err.println("Invalid Format: " + var3.getLocalizedMessage());
        }

        return value;
    }
}
