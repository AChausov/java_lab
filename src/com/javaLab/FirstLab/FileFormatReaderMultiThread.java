package com.javaLab.FirstLab;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class FileFormatReaderMultiThread extends FileFormatReader {
    private float resultValue;
    private int filesCount;

    FileFormatReaderMultiThread(int filesCountValue) {
        this.filesCount = filesCountValue;
        this.resultValue = 0.0F;
    }

    private synchronized void addValueToResult(float value) {
        this.resultValue += value;
    }

    public float calculatedValueFromFiles() throws IOException {
        ExecutorService service = Executors.newCachedThreadPool();

        for(int i = 0; i < this.filesCount; ++i) {
            final int fileNumber = i;
            service.submit(() -> {
                try {
                    BufferedReader input = new BufferedReader(new FileReader("in_" + fileNumber + ".dat"));
                    int actionType = Integer.parseInt(input.readLine());
                    String[] floatStrings = input.readLine().split(" ");
                    float[] floats = new float[floatStrings.length];

                    for(int j = 0; j < floats.length; ++j) {
                        floats[j] = Float.parseFloat(floatStrings[j]);
                    }

                    input.close();
                    switch(actionType) {
                        case 1:
                            this.addValueToResult(sumOfNumbers(floats));
                            break;
                        case 2:
                            this.addValueToResult(multipleOfNumbers(floats));
                            break;
                        case 3:
                            this.addValueToResult(sumOfSquares(floats));
                    }
                } catch (Exception var7) {
                    System.err.println("IOException while read files, " + var7.getLocalizedMessage());
                }

            });
        }

        service.shutdown();

        try {
            service.awaitTermination(1L, TimeUnit.MINUTES);
        } catch (InterruptedException var4) {
            System.err.println("InterruptedException while read files, " + var4.getLocalizedMessage());
        }

        return this.resultValue;
    }
}
