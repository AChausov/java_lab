package com.javaLab.FirstLab;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class FileFormatWriter {
    private int capacity;

    FileFormatWriter() {
        this.capacity = 0;
    }

    FileFormatWriter(int newCapacity) {
        this.capacity = newCapacity;
    }

    public void createFormatFiles() throws IOException {
        Random randomGenerator = new Random();

        for(int i = 0; i < this.capacity; ++i) {
            BufferedWriter writer = new BufferedWriter(new FileWriter("in_" + i + ".dat", false));
            writer.write(Integer.toString(randomGenerator.nextInt(3) + 1));
            writer.newLine();

            for(int j = 0; j < 2; ++j) {
                writer.write(Float.toString(randomGenerator.nextFloat() * 100.0F));
                writer.write(" ");
            }

            writer.close();
        }

    }

    public void createResultFile(float result, long duration) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter("out.dat", false));
        writer.write("Result - " + Float.toString(result));
        writer.newLine();
        writer.write("Duration - " + Long.toString(duration) + " ms");
        writer.newLine();
        writer.close();
    }
}
